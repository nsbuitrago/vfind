use bio::alphabets::dna::revcomp;
use flate2::read::GzDecoder;
use itertools::Itertools;
use memchr::memmem;
use parasail_rs::{Aligner, Matrix, Profile};
use polars::prelude::*;
use pyo3::prelude::*;
use seq_io::fastq::{Reader, Record};
use seq_io::parallel::parallel_fastq;
use std::fs::{self, File};
use std::io::Read;
use std::path::Path;

pub fn translate(seq: &[u8]) -> String {
    let mut peptide = String::with_capacity(seq.len() / 3);

    'outer: for triplet in seq.chunks_exact(3) {
        for c in triplet {
            if !c.is_ascii() {
                peptide.push('X');
                continue 'outer;
            }
        }

        let c1 = ASCII_TO_INDEX[triplet[0] as usize];
        let c2 = ASCII_TO_INDEX[triplet[1] as usize];
        let c3 = ASCII_TO_INDEX[triplet[2] as usize];

        let amino_acid = if c1 == 4 || c2 == 4 || c3 == 4 {
            'X'
        } else {
            AA_TABLE_CANONICAL[c1][c2][c3]
        };

        peptide.push(amino_acid);
    }
    peptide
}

/// https://www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi
/// U is equivalent to T here
///
/// The 1st index picks the 4x4 block
/// The 2nd index picks the row
/// the 3rd index picks the column
static AA_TABLE_CANONICAL: [[[char; 4]; 4]; 4] = [
    [
        ['K', 'N', 'K', 'N'], // AAA, AAC, AAG, AAU/AAT
        ['T', 'T', 'T', 'T'], // ACA, ACC, ACG, ACU/ACT
        ['R', 'S', 'R', 'S'], // AGA, AGC, AGG, AGU/AGT
        ['I', 'I', 'M', 'I'], // AUA/ATA, AUC/ATC, AUG/ATG, AUU/ATT
    ],
    [
        ['Q', 'H', 'Q', 'H'], // CAA, CAC, CAG, CAU/CAT
        ['P', 'P', 'P', 'P'], // CCA, CCC, CCG, CCU/CCT
        ['R', 'R', 'R', 'R'], // CGA, CGC, CGG, CGU/CGT
        ['L', 'L', 'L', 'L'], // CUA/CTA, CUC/CTC, CUG/CTG, CUU/CTT
    ],
    [
        ['E', 'D', 'E', 'D'], // GAA, GAC, GAG, GAU/GAT
        ['A', 'A', 'A', 'A'], // GCA, GCC, GCG, GCU/GCT
        ['G', 'G', 'G', 'G'], // GGA, GGC, GGG, GGU/GGT
        ['V', 'V', 'V', 'V'], // GUA/GTA, GUC/GTC, GUG/GTG, GUU/GTT
    ],
    [
        ['*', 'Y', '*', 'Y'], // UAA/TAA, UAC/TAC, UAG/TAG, UAU/TAT
        ['S', 'S', 'S', 'S'], // UCA/TCA, UCC/TCC, UCG/TCG, UCU/TCT
        ['*', 'C', 'W', 'C'], // UGA/TGA, UGC/TGC, UGG/TGG, UGU/TGT
        ['L', 'F', 'L', 'F'], // UUA/TTA, UUC/TTC, UUG/TTG, UUU/TTT
    ],
];

/// Maps an ASCII character to array index
///
/// A = 65, a = 97  => 0
/// C = 67, c = 99  => 1
/// G = 71, g = 103 => 2
/// T = 84, t = 116 => 3
/// U = 85, u = 117 => 3
static ASCII_TO_INDEX: [usize; 128] = [
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, // 0-15
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, // 16-31
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, // 32-47
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, // 48-63
    4, 0, 4, 1, 4, 4, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, // 64-79    (65 = A, 67 = C, 71 = G)
    4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, // 80-95    (84 = T, 85 = U)
    4, 0, 4, 1, 4, 4, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, // 96-111   (97 = a, 99 = c, 103 = g)
    4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, // 112-127  (116 = t, 117 = u)
];

/// Adapters flanking variable region
#[pyclass]
struct Adapters {
    #[pyo3(get, set)]
    left: Vec<u8>,
    #[pyo3(get, set)]
    right: Vec<u8>,
}

#[pymethods]
impl Adapters {
    /// Create a new Adapters instance
    #[new]
    fn new(left: String, right: String) -> Self {
        Adapters {
            left: left.into_bytes(),
            right: right.into_bytes(),
        }
    }

    /// Reverse complement the left and right adapter
    fn revcomp(&mut self) -> Self {
        Self {
            left: revcomp(&self.right),
            right: revcomp(&self.left),
        }
    }
}

/// Alignment parameters
#[derive(Debug, Clone)]
#[pyclass]
struct AlignParams {
    #[pyo3(get, set)]
    match_score: i32,
    #[pyo3(get, set)]
    mismatch_score: i32,
    #[pyo3(get, set)]
    gap_open_penalty: i32,
    #[pyo3(get, set)]
    gap_extend_penalty: i32,
    #[pyo3(get, set)]
    accept_alignment: f64,
}

/// use default alignment params
impl Default for AlignParams {
    fn default() -> Self {
        AlignParams {
            match_score: 3,
            mismatch_score: -2,
            gap_open_penalty: 5,
            gap_extend_penalty: 2,
            accept_alignment: 0.75,
        }
    }
}

/// macro for calculating accept_score
macro_rules! accept_align_threshold {
    ($align_params:expr, $adapters:expr) => {
        $adapters.len() as f64 * $align_params.match_score as f64 * $align_params.accept_alignment
    };
}

fn get_variants(
    adapters: &Adapters,
    fq_path: String,
    align_params: &AlignParams,
    aligner: &Aligner,
    is_reverse: bool,
) -> Vec<String> {
    let mut variants = Vec::new();
    let left_align_accept = accept_align_threshold!(align_params, adapters.left);
    let right_align_accept = accept_align_threshold!(align_params, adapters.right);

    let fq_file = File::open(fq_path).expect("Failed to open fastq file");
    let mut decoder = GzDecoder::new(fq_file);
    let mut decompressed = Vec::new();
    decoder
        .read_to_end(&mut decompressed)
        .expect("Failed to decompress fastq file");
    let reader = Reader::new(decompressed.as_slice());

    parallel_fastq(
        reader,
        4,
        2,
        |record, variant| {
            let seq = record.seq();
            let left_match = memmem::find(seq, &adapters.left);
            let right_match = memmem::rfind(seq, &adapters.right);

            let vr_start = if left_match.is_some() {
                let vr_start = left_match.unwrap() + adapters.left.len();
                Some(vr_start)
            } else {
                let left_alignment = aligner.semi_global_with_profile(seq);
                let left_score = left_alignment.get_score();
                if left_score as f64 >= left_align_accept {
                    let vr_start = left_alignment.get_end_ref();
                    Some(vr_start as usize)
                } else {
                    None
                }
            };

            let vr_end = if right_match.is_some() {
                let vr_end = right_match.unwrap();
                Some(vr_end)
            } else {
                let right_alignment = aligner.semi_global_with_profile(seq);
                let right_score = right_alignment.get_score();
                if right_score as f64 >= right_align_accept {
                    let vr_end =
                        right_alignment.get_end_ref() - right_alignment.get_length().unwrap();
                    Some(vr_end as usize)
                } else {
                    None
                }
            };

            if vr_start.is_some() && vr_end.is_some() {
                let vr_start = vr_start.unwrap();
                let vr_end = vr_end.unwrap();

                if is_reverse {
                    *variant = Some(revcomp(&seq[vr_start..vr_end]));
                } else {
                    *variant = Some(seq[vr_start..vr_end].to_vec());
                }
            }
        },
        |_, variant| {
            if let Some(variant) = variant {
                if variant.len() == 24 {
                    variants.push(translate(&variant));
                    // for i in (0..variant.len()).step_by(3) {
                    //     let codon = &variant[i..i + 3];
                    //     if codon[2] == b'G' || codon[2] == b'T' {
                    //         variants.push(translate(&variant));
                    //     }
                    // }
                }
            }
            None::<()>
        },
    )
    .unwrap();

    variants
}

/// Find variants flanked by adapter sequences
#[pyfunction]
fn find_variants(
    adapters: &Adapters,
    fq_1: String,
    fq_2: Option<String>,
    align_params: Option<&AlignParams>,
    save_path: Option<String>,
) -> PyResult<()> {
    let defalut_align_params = AlignParams::default();
    let align_params = align_params.unwrap_or(&defalut_align_params);
    let mut aligner = Aligner::new();
    aligner
        .gap_open(align_params.gap_open_penalty)
        .gap_extend(align_params.gap_extend_penalty)
        .vec_strategy("scan");

    let matrix = Matrix::create(
        b"ACGT",
        align_params.match_score,
        align_params.mismatch_score,
    );
    let fwd_aligner = aligner
        .profile(Profile::new(adapters.left.as_slice(), true, &matrix))
        .use_stats()
        .build();
    let rev_aligner = aligner
        .profile(Profile::new(adapters.right.as_slice(), true, &matrix))
        .use_stats()
        .build();
    let peptides = get_variants(&adapters, fq_1, align_params, &fwd_aligner, false);

    // if let Some(fq_2) = fq_2 {
    //     let rc_adapters = adapters.revcomp();
    // }
    let peptide_counts = peptides.into_iter().counts();
    let peptide_ids = peptide_counts.keys().collect::<Vec<_>>();
    let peptide_freq = peptide_counts.values().collect::<Vec<_>>();

    // convert peptide_ids into list of strings
    let peptide_ids = peptide_ids
        .iter()
        .map(|x| x.to_string())
        .collect::<Vec<_>>();
    // convert peptide_freqs into list of integers
    let peptide_freq = peptide_freq.iter().map(|x| **x as i64).collect::<Vec<_>>();

    // FIXME: just for debugging purposes
    peptide_ids.iter().for_each(|x| {
        println!("{}", x);
    });

    let mut df = DataFrame::new(vec![
        Series::new("variants", peptide_ids),
        Series::new("frequency", peptide_freq),
    ])
    .unwrap();

    // write a csv file with the peptide ids and their frequencies
    // should check if save_path is made
    if let Some(save_path) = save_path {
        let save_path = Path::new(&save_path);
        let mut out_file = File::options().write(true).create(true).open(save_path)?;

        let _ = CsvWriter::new(&mut out_file)
            //.include_header(true)
            .with_separator(b',')
            .finish(&mut df);
    };

    // FIXME: we should return the polars dataframe
    Ok(())
}

#[pymodule]
fn vfind(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<Adapters>()?;
    m.add_class::<AlignParams>()?;
    m.add_function(wrap_pyfunction!(find_variants, m)?)?;
    Ok(())
}
