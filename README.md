# vFind

A tool for fast variant finding from NGS data.

vFind recovers variable regions flanked by adapter sequences by checking for exact matches of adapters, and optionaly triggering a semiglobal alignment in case of non-exact matches.

## Contributing

To make a contribution, please fork this repo and create a new branch with your changes. Submit a pull request to be merged into the main branch.

